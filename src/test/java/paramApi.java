import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class paramApi {

    @Test
    public void parameteriseapi(){


        Response responsAsTshrtCollection = given()
                .contentType(ContentType.ANY)
                .param("id" , 12)
                .when()
                .get("https://reqres.in/api/users?page=2&id=12");
        System.out.println(responsAsTshrtCollection.body().asString());
    }
}
