import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PostApiRequest {

    @Test
    public void gettingFirstPutApi(){

        JSONObject requestParams = new JSONObject();
        requestParams.put("id", "1");
        requestParams.put("email", "Singh");
        requestParams.put("first_name", "sdimpleuser2dd2011");
        requestParams.put("last_name", "password1");
        requestParams.put("avatar", "https://reqres.in/img/faces/7-image.jpg");



       Response GettingPutResponse = given()
               .when()
               .body(requestParams.toJSONString())
               .post("https://reqres.in/api/users");

        int StatusCode  = GettingPutResponse.statusCode();
        Assert.assertTrue(StatusCode==201);
        System.out.println(GettingPutResponse.body().asString());
    }
}
